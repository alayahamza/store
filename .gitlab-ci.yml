variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  STORE_BACKEND_CONTAINER_NAME: store-backend
  STORE_BACKEND_IMAGE_NAME: alayahamza/store-backend:latest
  STORE_FRONTEND_CONTAINER_NAME: store-frontend
  STORE_FRONTEND_IMAGE_NAME: alayahamza/store-frontend:latest
  DOCKER_IMAGE_MAVEN: maven:3.6.3-openjdk-11-slim
  DOCKER_IMAGE_NODE: alayahamza/node-phantomjs
  DOCKER_IMAGE_DOCKER: docker
  ANSIBLE_DOCKER_IMAGE: alayahamza/ansible

stages:
  - QA
  - build
  - docker
  - deploy

sonarcloud-check:
  stage: QA
  image: $DOCKER_IMAGE_MAVEN
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify -Dmaven.test.skip=true sonar:sonar -f backend
  allow_failure: true

test-maven:
  stage: QA
  image: $DOCKER_IMAGE_MAVEN
  script:
    - mvn clean test -f backend

test-npm:
  stage: QA
  image: $DOCKER_IMAGE_NODE
  script:
    - cd frontend && npm ci
    - npm run test

build-maven:
  stage: build
  image: $DOCKER_IMAGE_MAVEN
  script:
    - mvn -Dmaven.test.skip=true clean package -f backend
  artifacts:
    paths:
      - backend/target/shop-api-0.0.1-SNAPSHOT.jar
    expire_in: 1 week

build-npm:
  stage: build
  image: $DOCKER_IMAGE_NODE
  script:
    - npm install -C frontend
    - npm run build-prod -C frontend
  artifacts:
    paths:
      - frontend/dist
      - frontend/nginx
    expire_in: 1 week

build-docker:
  stage: docker
  image: $DOCKER_IMAGE_DOCKER
  services:
    - docker:dind
  before_script:
    - docker info
  script:
    - pwd
    - ls -lrt backend/
    - ls -lrt frontend/
    - docker build -f backend/Dockerfile -t $STORE_BACKEND_IMAGE_NAME .
    - docker build -f frontend/Dockerfile -t $STORE_FRONTEND_IMAGE_NAME .
    - docker login -u $DOCKER_USER -p $DOCKER_PASS
    - docker push $STORE_BACKEND_IMAGE_NAME
    - docker push $STORE_FRONTEND_IMAGE_NAME

deploy_staging:
  stage: deploy
  image: $ANSIBLE_DOCKER_IMAGE
  before_script:
    - ansible --version
    - export ANSIBLE_HOST_KEY_CHECKING=False
    - "which ssh-agent || ( apt-get install -qq openssh-client )"
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$UBUNTU_SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ansible-playbook -i hosts playbook-start.yml --extra-vars "GITLAB_SSH_PRIVATE_KEY='$GITLAB_SSH_PRIVATE_KEY' HOST=staging"
  environment:
    name: staging
    url: cloud-ops.hopto.org
  when: manual
  only:
    - master

deploy_prod:
  stage: deploy
  image: $ANSIBLE_DOCKER_IMAGE
  before_script:
    - ansible --version
    - export ANSIBLE_HOST_KEY_CHECKING=False
    - "which ssh-agent || ( apt-get install -qq openssh-client )"
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$GCP_SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ansible-playbook -i hosts playbook-start.yml --extra-vars "GITLAB_SSH_PRIVATE_KEY='$GITLAB_SSH_PRIVATE_KEY' HOST=prod"
  environment:
    name: prod
    url: http://cloud-ops-store.ga
  when: manual
  only:
    - master
